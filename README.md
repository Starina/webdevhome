![Build and deploy website](https://github.com/webdevhome/webdevhome.github.io/workflows/Build%20and%20deploy%20website/badge.svg)

# &lt;WebdevHome /&gt;

[webdevhome.github.io](https://webdevhome.github.io)

This is a collection of links I as a web developer use very frequently. Maybe you will find them useful, too.

## Planned Features

- [x] Filter links by typing their name and open a page by pressing `Return`
- [x] Possibility to customize which links are visible
- [ ] Submit searches of supported sites from within this site (similar to Chrome's Omnibar)
- [ ] MOAR LINKS! \(°O°)/

## Credits

[TypeScript](https://github.com/microsoft/TypeScript)
• [React](https://github.com/facebook/react)
• [farzher/fuzzysort](https://github.com/farzher/fuzzysort)
• [Sass](https://github.com/sass/dart-sass)
• [Simple Icons](https://github.com/simple-icons/simple-icons)
• [Material Design Icons](https://github.com/Templarian/MaterialDesign)

---

Developed and maintained by: Andreas Linnert ([alinnert](https://github.com/alinnert))
